$(document).ready(function () {

    $('.js__text__draw').each(function (index, element) {
        let $el = $(this)
        let content = '';

        $el.find('span').each(function (index, element) {
            content += `<div class="text__draw__line"><span>${$(this).html()}</span></div>`;
        });

        $el.html('')
        $el.append(`<div class="js__text__draw--main text__draw--main">${content}</div>`)
        $el.append(`<div class="js__text__draw--clone text__draw--clone">${content}</div>`)






    })

        $('.js__text__draw--clone .text__draw__line').each(function () {
            let el = $(this)

            let elChilde = $(this).find('span')

            gsap.to(el, {
                scrollTrigger: {
                    trigger:  el[0],
                    start: 'top 60%',
                    end: `+=150`,
                    scrub: 1
                },
                x: '0%',
                ease: "ease-in-out",
                duration: 0.55,
                stagger: {
                    amount: 0.3
                }
            })

            gsap.to(elChilde, {
                scrollTrigger: {
                    trigger:  el[0],
                    start: 'top 60%',
                    end: `+=150`,
                    scrub: 1
                },
                x: '0%',
                ease: "ease-in-out",
                duration: 0.55,
                stagger: {
                    amount: 0.3
                }
            })
        })


	$('.__animate__top').each(function () {
		el = $(this)
		elParent = $(this).parent()[0]
        let delay =  $(this).data('delay') ? $(this).data('delay') : 0;
        let duration =  $(this).data('duration') ? $(this).data('duration') : 0.625;

		gsap.to(el, {
			scrollTrigger: {
				trigger: el[0],
				start: 'top 85%',
			},
			y: '0',
			opacity: 1,
			ease: 'ease-in-out',
            delay: delay,
            duration: duration
		})
	})

    $('.__animate__move__top').each(function () {
		el = $(this)
		elParent = $(this).parent()[0]
        let delay =  $(this).data('delay') ? $(this).data('delay') : 0;
        let duration =  $(this).data('duration') ? $(this).data('duration') : 0.625;

		gsap.to(el, {
			scrollTrigger: {
				trigger: el[0],
				start: 'top 70%',
			},
			y: '-100%',
			ease: 'ease-in-out',
            delay: delay,
            duration: duration
		})
	})






    $('.__animate__scale').each(function () {
		el = $(this)
		elParent = $(this).parent()[0]
        let delay =  $(this).data('delay') ? $(this).data('delay') : 0;
        let duration =  $(this).data('duration') ? $(this).data('duration') : 0.625;

		gsap.to(el, {
			scrollTrigger: {
				trigger:elParent,
				start: 'top 85%',
			},
			scale: 1,
			opacity: 1,
			ease: 'ease-in-out',
            delay: delay,
            duration: duration
		})
	})


    $('.__animate__left').each(function () {
		el = $(this)
		elParent = $(this).parent()[0]
        let delay =  $(this).data('delay') ? $(this).data('delay') : 0;
        let duration =  $(this).data('duration') ? $(this).data('duration') : 0.625;

		gsap.to(el, {
			scrollTrigger: {
				trigger: el[0],
				start: 'top 85%',
			},
			x: '0',
			opacity: 1,
			ease: 'ease-in-out',
            delay: delay,
            duration: duration
		})
	})


    $(".__animate__title").each(function () {
        el = $(this).find('span')
        elParent = $(this).parent()[0];
        let duration =  $(this).data('duration') ? $(this).data('duration') : 0.625;
        let delay =  $(this).data('delay') ? $(this).data('delay') : 0;

        gsap.to(el, {
            scrollTrigger: {
                trigger: elParent,
                start: 'top 85%',
            },
            y: '0',
            opacity:1,
            ease: "ease-in-out",
            stagger: {
                amount: 0.3
            },
            delay: delay,
            duration: duration

        })
    })



    $(".__animate__opacity").each(function () {
        el = $(this)
        elParent = $(this).parent()[0];
        let delay =  $(this).data('delay') ? $(this).data('delay') : 0;
        let duration =  $(this).data('duration') ? $(this).data('duration') : 0.625;

        gsap.to(el, {
            scrollTrigger: {
                trigger: el[0],
                start: 'top 85%',
            },
            y: '0',
            opacity:1,
            ease: "ease-in-out",
            duration:duration,
            delay: delay,
        })
    })
})
