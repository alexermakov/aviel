
$(function () {

    $(window).on("load", function () {
        let gmapStyle = [
            {
                "featureType": "water",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#d3d3d3"
                    }
                ]
            },
            {
                "featureType": "transit",
                "stylers": [
                    {
                        "color": "#808080"
                    },
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#b3b3b3"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#ffffff"
                    },
                    {
                        "weight": 1.8
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#d7d7d7"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#ebebeb"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#a7a7a7"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#efefef"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#696969"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#737373"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#d6d6d6"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {},
            {
                "featureType": "poi",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#dadada"
                    }
                ]
            }
        ]




        if ($('.js_contact__block__map_contact').length) {
            function gmap_contact() {

                let map;
                let locations = [];

                let $map = $('.js_contact__block__map_contact')[0];

                $('.js_contact__block__map_contact').each(function () {
                    let coord = $(this).attr('data-coord').split(', ')
                    locations.push({
                        lat: parseFloat(coord[0]),
                        lng: parseFloat(coord[1]),
                    })
                })

                map = new google.maps.Map($map, {
                    center: {
                        lat: parseFloat(locations[0].lat),
                        lng: parseFloat(locations[0].lng),
                    },
                    zoom: 10,
                    styles: gmapStyle,
                    mapTypeControl: false,
                    zoomControl: false,
                    scaleControl: false,
                    navigationControl: false,
                    streetViewControl: false,
                });

                const markers = locations.map((item, i) => {
                    const marker = new google.maps.Marker({
                        position: {
                            lat: item.lat,
                            lng: item.lng
                        },
                        map: map,
                        title: item.title,
                    });

                    return marker;
                });
            }
            gmap_contact()
        }



        if ($('.js_contact__block__map').length) {
            function gmap() {

                let map;
                let locations = [];

                let $map = $('.js_contact__block__map')[0];

                $('.js_contact__block__map').each(function () {
                    let coord = $(this).attr('data-coord').split(', ')
                    locations.push({
                        lat: parseFloat(coord[0]),
                        lng: parseFloat(coord[1]),
                    })
                })

                map = new google.maps.Map($map, {
                    center: {
                        lat: parseFloat(locations[0].lat),
                        lng:parseFloat(parseFloat(locations[0].lng) + 0.175),
                    },
                    zoom: 10,
                    styles: gmapStyle,
                    mapTypeControl: false,
                    zoomControl: false,
                    scaleControl: false,
                    navigationControl: false,
                    streetViewControl: false,
                });




                const markers = locations.map((item, i) => {
                    const marker = new google.maps.Marker({
                        position: {
                            lat: item.lat,
                            lng: item.lng
                        },
                        map: map,
                        title: item.title,
                    });

                    return marker;
                });



            }

            gmap()
        }
    })
})