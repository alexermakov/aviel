$(document).ready(function () {
	const dt = new DataTransfer()
	let $fileInput = $('.js_field_item--file input')
	$fileInput.change(function (e) {
		e.preventDefault()
		let $parent = $(this).closest('.js_field_item--file')
		let $listFiles = $parent.find('.field_item--file__list')

		let files = $(this).prop('files')

		$listFiles.html('')

		;[...files].forEach((file) => {
			$listFiles.append(`
                <span class='field_item--file__added js_field_item--file__added'>
                    <span class="field_item--file__added__name">${file.name}</span>
                    <span class="field_item--file__added__remove">+</span>
                </span>
            `)

		})

		for (let file of this.files) {
			dt.items.add(file)
		}

		this.files = dt.files

		$(document).on(
			'click',
			'.js_field_item--file__added .field_item--file__added__remove',
			function () {
				let el = $(this).closest('.js_field_item--file__added')
				let name = el.find('.field_item--file__added__name').text()
				$(this).parent().remove()
				for (let i = 0; i < dt.items.length; i++) {
					if (name === dt.items[i].getAsFile().name) {
						dt.items.remove(i)
						continue
					}
				}
				$fileInput[0].files = dt.files
			}
		)
	})

	$('.js_form_contact').submit(function (event) {

        $('.js_form_contact--message').remove()
        let $dateValue_el = $(this).find('.js_field_date')
        let $timeValue_el = $(this).find('.js_field_time')

        if ($(this)[0].checkValidity() && $dateValue_el.val()!=''  && $timeValue_el.val()!='') {
	        // let formData = $(this).serialize();
	        // let formDataAdmin = $(this).serializeArray();



            let $success_date_el = $('.js_contact__block_success__contact__item__value--date')
            let $success_time_el = $('.js_contact__block_success__contact__item__value--time')

            $success_date_el.text($dateValue_el.val())
            $success_time_el.text($timeValue_el.val())

            $('.js_contact__block_form').css('display','none')
            $('.js_contact__block_success').css('display','flex')

	        // $.ajax({
	        //     type: "POST",
	        //     url: "obr.php",
	        //     data: formData,
	        //     success: function (msg) {
	        //         // write_data_admin(formDataAdmin);
	        //           Fancybox.show([{src: "#js_modal_success"}])
	        //     }
	        // });
	    }else{
            $(this).append(`<div class="form_contact--message js_form_contact--message">The form is filled out incorrectly, you need to add the date and time</div>`)
        }
	});
})
