$(function () {

    $(window).on("load", function () {
        $('body').addClass('loaded')
    })


    $('.js_go_to_form').click(function(e){
        e.preventDefault();
        $('html,body').animate({scrollTop: $('.js_contact__block_form').offset().top - ($(window).height() / 10)},'slow');
    })

    if ($('.js_grid').length){
        $('.js_grid').masonry({
            // options
            itemSelector: '.page_review__item',
        });
    }


    $(".js_field_time").flatpickr({
        enableTime: true,
        noCalendar: true,
        minuteIncrement:30,
        dateFormat: "H:i",
    });


    $(".js_field_date").flatpickr({
        altInput: true,
        altFormat: "F j, Y",
        dateFormat: "Y-m-d",
        minDate: "today"
    });


    $('.js_review_home_list').slick({
        dots: false,
        arrows: false,
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        swipeToSlide:true,
        responsive: [
            {
              breakpoint: 1023,
              settings: {
                variableWidth: true,
                slidesToShow: 1,
              }
            }
          ]
    })

    $('.js_faq_item .faq_item__top').click(function (e) {
        e.preventDefault();
        let faqItem = $(this).closest('.js_faq_item')
        faqItem.toggleClass('active')
        faqItem.find('.faq_item__content').slideToggle(300)
    });



    Fancybox.bind('.js__modal', {
        autoFocus: false,
        trapFocus: false,
        touch: false,
        closeButton: 'outside',
    });

    Fancybox.defaults.Thumbs = false;
    $(".js_select").each(function () {
        let placeholder = $(this).attr('placeholder');
        $(this).select2({
            minimumResultsForSearch: 1 / 0,
            placeholder: placeholder,
            // allowClear: true
        })
    });


    $('.js__btn_close__modal').click(function (e) {
        Fancybox.close();
    });


    $('.js__btn_open__modal').click(function (e) {
        Fancybox.show([{
            src: "#js_modal__call"
        }])
    });


    $('.js_btn_menu,.js_modal__menu__btn_close,.js_modal__menu_overlay').click(function (e) {
        e.preventDefault();
        $('.js_btn_menu').toggleClass('active')
        $('.js__modal__menu').toggleClass('active')
        $('.js_modal__menu_overlay').toggleClass('active')
    });




});